package application;
import javax.swing.*;
import resources.*;
import controllers.*;

public class application {
	
	private static BuildingsController buildingsctrl = new BuildingsController();
	
	
	public static void main(String[] args) {
		JFrame mainFrame = new JFrame();
		//Buildings buttons
		JButton updateCastle = new JButton("Update Castle");
		final JTextField castleLevel = new JTextField();
		castleLevel.setBounds(300,300,150,20);
		//Adding buildings buttons to mainFrame
		mainFrame.add(updateCastle);
		mainFrame.add(castleLevel);
		updateCastle.setBounds(0,0,300,50);
		updateCastle.addActionListener(e -> {
			castleLevel.setText(Integer.toString(buildingsctrl.castleLevel() + 1));
			System.out.println(buildingsctrl.castleLevel());
		});
		//mainFrame settings
		mainFrame.setSize(1920, 1080);
		mainFrame.setLayout(null);
		mainFrame.setVisible(true);
		
	}
}
