package resources;

public class Resources {

	public int total_wood = 20;
	public int total_gold = 200;
	public int total_stone = 10;
	public int total_iron = 5;
	public int total_food = 100;
	public int total_population = 5;
	public int total_faith = 2;
	
	public int getTotal_wood() {
		return total_wood;
	}
	public void setTotal_wood(int total_wood) {
		this.total_wood = total_wood;
	}
	public int getTotal_gold() {
		return total_gold;
	}
	public void setTotal_gold(int total_gold) {
		this.total_gold = total_gold;
	}
	public int getTotal_stone() {
		return total_stone;
	}
	public void setTotal_stone(int total_stone) {
		this.total_stone = total_stone;
	}
	public int getTotal_iron() {
		return total_iron;
	}
	public void setTotal_iron(int total_iron) {
		this.total_iron = total_iron;
	}
	public int getTotal_food() {
		return total_food;
	}
	public void setTotal_food(int total_food) {
		this.total_food = total_food;
	}
	public int getTotal_population() {
		return total_population;
	}
	public void setTotal_population(int total_population) {
		this.total_population = total_population;
	}
	public int getTotal_faith() {
		return total_faith;
	}
	public void setTotal_faith(int total_faith) {
		this.total_faith = total_faith;
	}
	
}
