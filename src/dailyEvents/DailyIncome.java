package dailyEvents;

import resources.*;
import controllers.*;

public class DailyIncome {

	private Resources res;
	private BuildingsController ctrl;
	
	/*1 gold from every citizen*/
	public int getDailyGoldIncomeFromPopulation() {
		return this.res.getTotal_population() * 1;
	}
	
	public int getDailyGoldIncomeFromCastle() {
		return ctrl.castleLevel() * 100;
	}
	
	
}
