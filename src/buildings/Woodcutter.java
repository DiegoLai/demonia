package buildings;
import resources.*;
public class Woodcutter implements Building {
	private int currentLevel = 1;
	private int foodPrice = 150;
	private int stonePrice = 50;
	private int woodPrice = 0;
	private int ironPrice = 0;
	private int goldPrice = 0;
	
	private Resources res;
	
	@Override
	public void upgrade() {
		if(res.getTotal_food() >= this.foodPrice && this.res.getTotal_stone() >= this.stonePrice && this.getLevel() < MAX_LEVEL) {
			res.setTotal_food(res.getTotal_food() - this.foodPrice); 
			updateCost();
			this.currentLevel = this.getLevel() + 1;	
		}
		
	}

	@Override
	public void updateCost() {
		this.foodPrice *= 2;
		this.stonePrice *=2;
	}

	@Override
	public int getLevel() {
		return this.currentLevel;
	}

	@Override
	public int getFoodPrice() {
		return this.foodPrice;
	}
	
	@Override
	public int getWoodPrice() {
		return this.woodPrice;
	}

	@Override
	public int getStonePrice() {
		return this.stonePrice;
	}

	@Override
	public int getIronPrice() {
		return this.ironPrice;
	}

	@Override
	public int getGoldPrice() {
		return this.goldPrice;
	}

}
