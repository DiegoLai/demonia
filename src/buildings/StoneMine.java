package buildings;

import resources.*;

public class StoneMine implements Building {
	
	private int currentLevel = 1;
	private int foodPrice = 0;
	private int stonePrice = 0;
	private int woodPrice = 300;
	private int ironPrice = 0;
	private int goldPrice = 0;
	private Resources res;
	
	@Override
	public void upgrade() {
		if(res.getTotal_wood() >= this.woodPrice && this.getLevel() < MAX_LEVEL) {
			res.setTotal_wood(res.getTotal_wood() - this.woodPrice); 
			updateCost();
			this.currentLevel = this.getLevel() + 1;	
		}
	}
	
	@Override
	public void updateCost() {
		this.woodPrice *= 2;
	}

	@Override
	public int getLevel() {
		return this.currentLevel;
	}

	@Override
	public int getFoodPrice() {
		return this.foodPrice;
	}
	
	@Override
	public int getWoodPrice() {
		return this.woodPrice;
	}

	@Override
	public int getStonePrice() {
		return this.stonePrice;
	}

	@Override
	public int getIronPrice() {
		return this.ironPrice;
	}

	@Override
	public int getGoldPrice() {
		return this.goldPrice;
	}

}
