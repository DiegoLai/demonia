package buildings;

public interface Building {
	
	int MAX_LEVEL = 10;
	// Used to upgrade the level of a building
	public void upgrade();
	// Used to upgrade the cost of a building
	public void updateCost();
	// Used to get the level of a building
	public int getLevel();
	// Used to get the cost of a building
	public int getFoodPrice();
	public int getWoodPrice();
	public int getStonePrice();
	public int getIronPrice();
	public int getGoldPrice();
}
