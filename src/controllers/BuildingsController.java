package controllers;

import buildings.*;

public class BuildingsController {	
	
	Castle castle = new Castle();
	Farms farms = new Farms();
	Houses houses = new Houses();
	IronMine ironmine = new IronMine();
	StoneMine stonemine = new StoneMine();
	Woodcutter woodcutter = new Woodcutter();
	
	public int castleLevel() {
		return this.castle.getLevel();
	}
	
	public int farmsLevel() {
		return this.farms.getLevel();
	}
	
	public int housesLevel() {
		return this.houses.getLevel();
	}
	
	public int ironmineLevel() {
		return this.ironmine.getLevel();
	}
	
	public int stonemineLevel() {
		return this.stonemine.getLevel();
	}
	
	public int woodcutterLevel() {
		return this.woodcutter.getLevel();
	}

}
